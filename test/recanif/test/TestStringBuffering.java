/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recanif.test;

import java.io.BufferedReader;
import static java.time.Clock.system;
import java.util.Arrays;
import java.util.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ryansalge
 */
public class TestStringBuffering {

    private static String[] split(String property) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public TestStringBuffering() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testBufferingAlgorithm() {
        System.err.println("I ran my unit test");
        String[] inputs = {"[1,2,3]\n", "[4,", "5,6]\n", "[7,8,9]\n[10,11", ",12]\n"};
        String[] tmp = {"[1,2,3]", "[4,5,6]", "[7,8,9]", "[10,11,12]"};
        Vector< String> expected = new Vector< String>();
        for (String x : tmp) {
            expected.add(x);
        }
        Vector< String> outputs = new Vector<>();

        StringBuilder builder = new StringBuilder();

        for (String i : inputs) {
            // Do something
            for (char ch : i.toCharArray()) {
                // If you find a matching line, do:
                // outputs.add(line);
                if (ch != '\n') {
                    builder.append(ch);
                } else {
                    outputs.add(builder.toString());
                    builder.setLength(0);
                }
            }
        }
        System.out.println("Hello");
        assert (outputs.size() == expected.size());
        // Idea: each element in outputs should match the corresponding element
        // in expected
    }
}
