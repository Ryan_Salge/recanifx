package recanifx;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ryansalge
 */
public interface ArduinoListener {
    public void onConnect(String portName);
    public void onMessage (String line);
    public void onError(String message);
    public void onClose();
}
