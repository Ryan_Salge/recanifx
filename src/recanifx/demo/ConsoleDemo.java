/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recanifx.demo;

import recanifx.ArduinoConnection;
import recanifx.ArduinoListener;

/**
 *
 * @author ryansalge
 */
public class ConsoleDemo implements ArduinoListener {

    private String portName = null;
    private boolean open = false;

    public void onConnect(String portName) {
        this.portName = portName;
        open = true;
    }

    public void onMessage(String line) {
        System.out.println(line);
        //Platform.later(...);
    }

    public void onError(String message) {
        System.err.println(message);
    }

    public synchronized void onClose() {
        open = false;
        this.notify();
    }

    public synchronized boolean isOpen() {
        return this.open;
    }

    public static void main(String[] args) {
        try {
            // TODO: if (args.length == 0) then print error message and 
            // list all port names.
            System.out.println("Hello World");
            ArduinoConnection conn = new ArduinoConnection();
            ConsoleDemo listener = new ConsoleDemo();
            // TODO: Check return value of connect()
            conn.connect(args[0], listener);
            while (listener.isOpen()) {
                synchronized (listener) {
                    try {
                        listener.wait();
                    } catch (InterruptedException ex) {
                        System.err.println("Interrupted");
                        break;
                    }
                }
            }
        } catch (Throwable t) {
            System.err.println("Uncaught exception: " + t);
        }
    }

}
