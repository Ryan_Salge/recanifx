package recanifx;

import recanifx.ArduinoListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import static jssc.SerialPort.MASK_RXCHAR;
import jssc.SerialPortEvent;
import jssc.SerialPortException;
import jssc.SerialPortList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ryansalge
 */
public class ArduinoConnection {

    private SerialPort arduinoPort = null;
    private ArduinoListener listener = null;
    StringBuilder builder = new StringBuilder();

    public String[] getPortNames() {
        return SerialPortList.getPortNames();
    }

    public boolean isConnected() {
        return arduinoPort != null;
    }

    public boolean connect(String portName, final ArduinoListener listener) {
        if (arduinoPort != null) {
            throw new RuntimeException("Already Connected");
        }
        System.out.println("connectArduino");

        boolean success = false;
        SerialPort serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(
                    SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.setEventsMask(MASK_RXCHAR);
            serialPort.addEventListener((SerialPortEvent serialPortEvent) -> {
                if (serialPortEvent.isRXCHAR()) {
                    try {
                        String str = serialPort.readString(serialPortEvent
                                .getEventValue());

                        for (char ch : str.toCharArray()) {
                            // If you find a matching line, do:
                            // outputs.add(line);
                            if (ch != '\n') {
                                // Ignore the \r
                                if (ch != '\r') {
                                    builder.append(ch);
                                }
                            } else {
                                String line = builder.toString();
                                builder.setLength(0);
                                listener.onMessage(line);

                            }
                        }

                    } catch (SerialPortException ex) {
                        listener.onError(ex.getMessage());
                        disconnect();
                    }

                }
            }
            );

            this.listener = listener;
            listener.onConnect(portName);
            arduinoPort = serialPort;
            success = true;
        } catch (SerialPortException ex) {
            Logger.getLogger(ArduinoConnection.class.getName())
                    .log(Level.SEVERE, null, ex);
            System.out.println("SerialPortException: " + ex.toString());
        }

        return success;
    }

    public void disconnect() {
        if (arduinoPort != null) {
            try {
                arduinoPort.removeEventListener();

                if (arduinoPort.isOpened()) {
                    arduinoPort.closePort();
                    listener.onClose();
                }
                arduinoPort = null;
                listener = null;
                builder.setLength(0);
            } catch (SerialPortException ex) {
                Logger.getLogger(ArduinoConnection.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return;

    }
}
